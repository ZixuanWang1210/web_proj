let list=[];
let idx=1;
import {db} from './webSQL.js';

let book={
    id:'defult',
    name:'defult',
    isbn:'defult',
    price:'defult',
    pub:'defult'
}

// 将添加按钮中文字修改为 添加
// $('.change2add').on('click', function(e){
//     console.log("change to add succeed");
//     $('#myModalLabel').text("添加图书");
//     $('.myAdd').text("添加");
// })

// 更新列表信息
function updateList(){
    $("tbody").children().remove();
    for(let i=0;i<list.length;i++){
        $("tbody").append($(
            `<tr>
            <th scope="row">${i}</th>
            <td>${list[i].name}</td>
            <td>${list[i].isbn}</td>
            <td>${list[i].price}</td>
            <td>${list[i].pub}</td>
            <td>
            <button type="button" class="btn btn-default">
            <span class="glyphicon glyphicon-eye-close hideTag"></span>
            </button> &nbsp;
            <button type="button" class="btn btn-default myEdit" data-toggle="modal" data-target="#editBook">
            <span class="glyphicon glyphicon-edit"></span>
            </button>
            &nbsp;
            <button type="button" class="btn btn-default myDel" >
            <span class="glyphicon glyphicon-trash"></span>
            </button>
            </td>
            </tr>`
        ))
    }
}


// 点击添加按钮修改文字 并添加信息
$('.myAdd').on('click', function (e) {
    console.log("ready to add new book");
    let bkName=$("#bookname").val();
    let bkISBN=$("#ISBN").val();
    let bkPrice=$("#bookprice").val();
    let bkPub=$("#bookpub").val();
    let $tbody=$("tbody");

    let book={
        id:'defult',
        name:'defult',
        isbn:'defult',
        price:'defult',
        pub:'defult'
    }

    book.isbn=bkISBN;
    book.price=bkPrice;
    book.pub=bkPub;
    book.name=bkName;
    console.log(book);

    list.push(book);
    console.log(list);
    // updateList();
    upBookList();

    db.transaction(function (tx) {
        tx.executeSql("insert into book(Bno,Bname,BISBN,Bprice,Bpub) VALUES (?,?,?,?,?)", [idx,bkName,bkISBN,bkPrice,bkPub],
            function(){
                console.log("add book succeed!!!");
            }
        );
        idx++;
    });
    upBookList();
})

// 隐藏按钮
$('.table').on('click','.hideTag',function(){
    $(this).closest('tr').remove();
})

// TO-DO 无法选中 myDel
$('.myDel').on('click',function(){
    let id=$(this).closest('tr').firstChild;
    console.log(id);
})

// 表格内单击修改按钮时 修改行内信息
// TO-DO 修复首行
// $('.myEdit').on('click',function(e){
//     console.log("change to edit succeed!");
//     let $myEdit=$('.myEdit').html();
    
//     $('#myModalLabel').text("修改图书");
//     $('.myAdd').text("修改");
// })

var res = template("mytplt", []);

function upBookList(){
    var querycontent = $('#keyword').val();
		var sqlquery = "SELECT * FROM book where Bname like ? ";
		db.transaction(function (tx) {
			tx.executeSql(sqlquery, ["%" + querycontent + "%"], function (tx, result) {
				var len = result.rows.length;
                // console.log(result);
				//先清空表格
				$("#booklist").empty();   //从被选元素移除所有内容,包括所有文本和子节点
				var arr = { data: [] };
				for (var i = 0; i < len; i++) {
					arr.data.push(result.rows.item(i));
				}
				var res = template("mytplt", arr);
				// 将模板放入页面中
                // console.log(res);
                // console.log(res);
				$("#booklist").html(res);

				//必须在页面加载后才能添加事件
				$("#booklist td button:nth-of-type(2)").on("click", function () {
					var id = $(this).attr("bid");
					var r = confirm("是否要删除图书信息？")
					if (r == true) {
						deletebook(id);
					}

				});

				//修改						
				// $("#booklist td button:nth-of-type(1)").on("click", function (event) {
				// 	//							event.preventDefault(); 
				// 	var id = $(this).attr("bid");
				// 	var row = $(this).parent().siblings("td");  //获取数据
				// 	book = {
				// 		id: id,
				// 		name: $(row[1]).html(),
				// 		ISBN: $(row[2]).html(),
				// 		isbn: $(row[3]).html(),
				// 		pub: $(row[4]).html()
				// 	};

				// });
			},
				function (tx, error) {
					alert('查询失败: ' + error.message);
				});
		});
}


// $("#editBook .modal-footer .btn-primary").on("click", function () {
//     updatebook();
//     $("#editBook").modal('hide');

// });

$('.btn-edit-inline').on('click',function(e){

    // TO DO
    console.log("ok");
    let id=$(this).attr('bid');
    book.id=id;
    console.log(id);
})

$('#myEdit').on('click',function(){
    updateBook();
    $("#editBook").modal('hide');
    // 注意引入方式
});

function updateBook(){
    let bkName=$("#bookname").val();
    let bkISBN=$("#ISBN").val();
    let bkPrice=$("#bookprice").val();
    let bkPub=$("#bookpub").val();
    
    book.isbn=bkISBN;
    book.price=bkPrice;
    book.pub=bkPub;
    book.name=bkName;
    console.log(book);
}
